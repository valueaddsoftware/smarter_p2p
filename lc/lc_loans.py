from lc_loan import *

class LcLoans:

  def __init__(self, loans):
    self.loans = [] if loans is None else list(map((lambda loan: LcLoan(loan)), loans))

  def contains_loan(self, listing_number):    
    for loan in self.loans:
      if loan.has_listing_number(listing_number):
        return True
        

  def add_loans (self, new_loans):
    #print self.loans
    #print new_loans
    self.loans += new_loans
    #print loans

    #84058