from lc_loan import *
from datetime import datetime, tzinfo
from utc import UTC

class LcFilter:

  grades = ["D", "E", "F", "G"]
  good_purposes = ["credit_card", "debt_consolidation", "home_improvement", "other"]
  home_ownership = ["OWN", "MORTGAGE"]
  
  @staticmethod
  def passes_aggressive_filter(loan):
    return (not loan.annual_income is None and 
    	not loan.employment_length is None and
    	loan.annual_income >= 106000 and
    	loan.grade in LcFilter.grades and
    	loan.purpose in LcFilter.good_purposes and
    	loan.inquiries_in_last_6_months <= 2 and
    	loan.employment_length >= 60 and
      loan.home_ownership in LcFilter.home_ownership and
      loan.bankcard_util >= 55 and
      loan.earliest_credit_line <= datetime(2001,01,01,tzinfo=UTC()))

