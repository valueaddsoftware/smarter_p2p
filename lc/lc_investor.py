import datetime
from datetime import timedelta
from lc_user import *
from lc_api import *
from lc_filter import *

class LcInvestor:


  def __init__(self):
    self.api = LcApi()
    self.all_users = LcUser.get_all_users(self.api)

  def run_investment_job(self, all, job_run_time=30):
    users_who_can_invest = []
    for lc_user in self.all_users:
      print str(lc_user)
      if not lc_user.account.can_invest():
        print "%s cannot invest" % (lc_user.name)
      else:
        print "Adding %s into users who can invest" %  (lc_user.name)
        users_who_can_invest.append(lc_user)

    if not users_who_can_invest:
      print "There are no users that can invest"
      return

    start_time = datetime.now()
    while (datetime.now() - start_time) < timedelta(seconds=job_run_time): #30
      self.invest_in_listings(users_who_can_invest, all)


  def invest_in_listings(self, users_who_can_invest, all):
      listings = self.api.get_listings(all)
      print "Total number of listings: %s at time: %s" % (len(listings.loans), str(datetime.now()))

      #For each listing, see if user can be invested
      for listing in listings.loans:
        if not LcFilter.passes_aggressive_filter(listing):
          continue
        print "Found listing %s - %s, %s, %s, %s, %s" % (listing.listing_number, listing.grade, listing.annual_income, listing.purpose, listing.inquiries_in_last_6_months, listing.employment_length)

        for lc_user in users_who_can_invest:
          if lc_user.investments.contains_loan(listing.listing_number):
            print "%s already invested in listing %s" % (lc_user.name, listing.listing_number)
            continue
          print "Adding listing %s to order for user %s" % (listing.listing_number, lc_user.name)
          lc_user.add_to_order(listing)

      for lc_user in users_who_can_invest:
        lc_user.complete_order()
