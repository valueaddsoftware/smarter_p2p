from lc_investor import *
from datetime import datetime


def lambda_handler(event, context):
    print('Running Lc investor at {}...'.format(event['time']))
    lc_investor = LcInvestor()
    lc_investor.run_investment_job(False)
    print('Lc Investor completed at {}'.format(str(datetime.now())))