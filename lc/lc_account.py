#require '../common/account.rb'
from account import *

class LcAccount(Account):

  def __init__(self, account, api_user_id, api_key, per_listing_investment_amount, max_account_investment_threshold):
    Account.__init__(self, account.get('availableCash'), account.get('infundingBalance'), account.get('outstandingPrincipal'), account.get('accountTotal'), api_user_id, api_key, per_listing_investment_amount, max_account_investment_threshold)
    self.netAnnualizedReturnAdjusted = account.get('netAnnualizedReturn').get('combinedAdjustedNAR')


''' JSON object structure
{
  "investorId": 12345,
  "availableCash": 1950096.7,
  "accountTotal": 2418863.35,
  "accruedInterest": 12758.85,
  "infundingBalance": 6225,
  "receivedInterest": 329272.5,
  "receivedPrincipal": 738356.23,
  "receivedLateFees": 75.31,
  "outstandingPrincipal": 462541.65,
  "totalNotes": 10628,
  "totalPortfolios": 19,
  "netAnnualizedReturn": {
    "primaryNAR": 0.1044,
    "primaryAdjustedNAR": 0.0476,
    "primaryUserAdjustedNAR": 0.0492,
    "tradedNAR": 0.0671,
    "tradedAdjustedNAR": 0.0671,
    "tradedUserAdjustedNAR": 0.0671,
    "combinedNAR": 0.1043,
    "combinedAdjustedNAR": 0.0476,
    "combinedUserAdjustedNAR": 0.0492
  },
  "adjustments": {
    "adjustmentForPastDueNotes": 104348.71,
    "userAdjustmentForPastDueNotes": 54098.07
  }
}
'''
