from math import floor

class Account:
	#attr_accessor :available_balance, :pending_investment_value, :active_investment_value,
	#	:total_account_value, :per_listing_investment_amount, :max_account_investment,
  #  :api_user_id, :api_key
	#:pending_quick_invest_orders, :TotalPrincipalReceivedOnActiveNotes, :OutstandingPrincipalOnActiveNotes

  def __init__(self, available_balance, pending_investment_value, active_investment_value,
    total_account_value, api_user_id, api_key, per_listing_investment_amount, max_account_investment):
    self.available_balance = available_balance
    self.pending_investment_value = 0 if pending_investment_value is None else pending_investment_value
    self.active_investment_value = active_investment_value
    self.total_account_value = total_account_value
    self.per_listing_investment_amount = per_listing_investment_amount
    self.max_account_investment = total_account_value if max_account_investment is None else max_account_investment

  def __str__(self):
    return "Available Blance: %s, Pending Investments/Id: %s, Active Investments: %s, Total Account Value: %s, Per Listing Investment Amount: %s, Max Account Investments :%s, Can Invest: %s" % (self.available_balance,
    self.pending_investment_value, self.active_investment_value, self.total_account_value, self.per_listing_investment_amount, self.max_account_investment, self.can_invest())

  def can_invest(self):
    return (self.available_balance >= self.per_listing_investment_amount and (self.pending_investment_value + self.active_investment_value) < (self.max_account_investment - self.per_listing_investment_amount))

  def num_of_possible_investments(self):
    return int(floor((min(self.max_account_investment, self.total_account_value) - (self.active_investment_value + self.pending_investment_value)) / self.per_listing_investment_amount))
