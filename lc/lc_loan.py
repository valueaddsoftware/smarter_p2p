#from datetime import *
import dateutil.parser

class LcLoan:
  def __init__(self, loan): 
    self.loan = loan
    self.invested_listing_number = loan.get('loanId')
    self.listing_number = loan.get('id')
    self.listing_title = loan.get('ListingTitle')
    self.annual_income = loan.get("annualInc")
    self.grade = loan.get("grade")
    self.purpose = loan.get("purpose")
    self.inquiries_in_last_6_months = loan.get("inqLast6Mths")
    self.employment_length = loan.get("empLength")
    self.bankcard_util = loan.get("bcUtil")
    self.earliest_credit_line = dateutil.parser.parse(loan.get("earliestCrLine")) if loan.get("earliestCrLine") != None else None
    self.home_ownership = loan.get("homeOwnership")

  def has_listing_number(self, loan_id):
    return self.invested_listing_number == loan_id or self.listing_number == loan_id

  def print_loan_details(self):
    print "Invested Listing Number/LoanId: %s, Listing Number/Id: %s, Title: %s, Annual Income: %s, Grade: %s, Purpose: %s" % (self.invested_listing_number, self.listing_number, self.listing_title, self.annual_income, self.grade, self.purpose)
    #, , Last 6 Months Inquiries: %s, Employment Length: %s, Bankcard Utilisation: %s, Home ownership: %s" % (, self.inquiries_in_last_6_months, self.employment_length, self.bankcard_util, self.home_ownership)
  
  #DateTime.parse(loan["earliestCrLine"))
    


  

