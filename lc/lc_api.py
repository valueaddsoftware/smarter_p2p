from lc_loans import *
import urllib2
import urllib
import json

class LcApi:

  root_url = 'https://api.lendingclub.com/api/investor/v1/'
  base_authorization = "D4WOiDX3jN1rWOv5mD8D+d1J55Y="
  

  def get_listings(self, all):
    url_verb = "loans/listing/" 
    params = { "showAll" : "true" } if all else None
    return LcLoans(self.get_with_basic_auth(url_verb, self.base_authorization, params)["loans"])

  def get_invested_listings(self, lc_user):
    url_verb = "accounts/" + lc_user.account_id + "/notes"
    return LcLoans(self.get_with_basic_auth(url_verb, lc_user.authorization, {})["myNotes"])
  

  def invest_in_listings(self, lc_user, orders):
    url_verb = "accounts/" + lc_user.account_id + "/orders"
    return self.post_with_basic_auth(url_verb, lc_user.authorization, {'aid' : lc_user.account_id, 'orders' : orders})
  

  def create_portfolio(self, lc_user, portfolio_name, portfolio_description):
    url_verb = "accounts/" + lc_user.account_id + "/portfolios"
    return self.post_with_basic_auth(url_verb, lc_user.authorization, {'aid' : lc_user.account_id, 'portfolioName' : portfolio_name, 'portfolioDescription' : portfolio_description})
  

  def get_account(self, lc_user):
    url_verb = "accounts/" + lc_user.account_id + "/summary"
    return self.get_with_basic_auth(url_verb, lc_user.authorization, {})


  def get_with_basic_auth(self, url_verb, authorization, parameters):  
    api_url = self.root_url + url_verb
    api_url += '?' + urllib.urlencode(parameters) if parameters else ''
    req = urllib2.Request(api_url, headers=self.generate_headers(authorization))
    return json.load(urllib2.urlopen(req))


  def post_with_basic_auth(self, url_verb, authorization, form_data):
    headers = {'Content-Type': 'application/json'}
    headers.update(self.generate_headers(authorization))
    req = urllib2.Request(self.root_url + url_verb, json.dumps(form_data), headers)
    return urllib2.urlopen(req)
     

  def generate_headers(self, authorization):
    return {"Authorization" : authorization, "X-LC-Application-Key" : "2TNqeLPckGhsWGvw5NOw1kHpJ/8="}



