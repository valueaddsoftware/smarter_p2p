from datetime import timedelta, datetime, tzinfo

class UTC(tzinfo):
	
  def utcoffset(self, dt):
    return timedelta(0)

  def dst(self, dt):
	return timedelta(0)
	
	def tzname(self,dt):
 	  return "UTC"