from lc_account import *

class LcUser:

  #attr_reader :name, :account_id, :authorization, :investments, :investment_amount, :account

  def __init__(self, name, account_id, authorization, portfolio_id, investment_amount, lc_api, max_account_investment):
    self.name = name
    self.account_id = account_id
    self.authorization = authorization
    self.lc_api = lc_api
    self.investments = self.lc_api.get_invested_listings(self)
    #for loan in self.investments.loans:
    #  loan.print_loan_details()
    self.investment_amount = investment_amount
    account_json = lc_api.get_account(self)
    self.account = LcAccount(account_json, account_id, authorization, investment_amount, max_account_investment)
    self.portfolio_id = portfolio_id
    self.order = []


  def add_order_to_investments(self):
    "Print adding current order to investments"
    self.investments.add_loans( self.order )
    self.order = []

  def add_to_order(self, lc_loan):
    self.order.append(lc_loan)

  def generate_order_form_data(self):
    #list(map((lambda x: x **2), items))
    return list(map((lambda loan: { "loanId" : loan.listing_number, "requestedAmount" : self.investment_amount , "portfolioId" : self.portfolio_id}), self.order))

  def complete_order(self):
    if not self.order:
      #print "User %s has an empty order" % (self.name)
      return
    elif len(self.order) > self.account.num_of_possible_investments():
        print "User %s has order size %s greater than number of possible investments %s" % (self.name, len(self.order), self.account.num_of_possible_investments())
        self.order = self.order[:self.account.num_of_possible_investments()]
        print "Reduced size of order to %s" % (len(self.order))
    response = self.lc_api.invest_in_listings(self, self.generate_order_form_data())
    print "%s successfully invested with status %s" % (self.name, response.read())
    self.add_order_to_investments()

  def __str__(self):
    return "User %s - %s, %s" % (self.name, self.account_id, str(self.account))

  @staticmethod
  def get_all_users(lc_api):
    users = [LcUser("Varun", "28792371", "D4WOiDX3jN1rWOv5mD8D+d1J55Y=", 52062023, 25, lc_api, 0),
             LcUser("Colin", "49566116", "/4SGNo4MnowkfEfx6JVt1X1qF8c=", 52052020, 25, lc_api, None),
             LcUser("Jeff", "57514829", "zYZf++5VJX/6Bi3J/x4Z/tYGMnM=", 56412024, 25, lc_api, 3000),
             LcUser("Eric", "1891094", "7FZviIQED0MewQbcDDR/okupJPE=", 56412026, 25, lc_api, None)]
    return users
