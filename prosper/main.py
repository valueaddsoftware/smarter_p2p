from prosper_investor import *
from datetime import datetime


def lambda_handler(event, context):
    print('Running Prosper investor at {}...'.format(event['time']))
    prosper_investor = ProsperInvestor()
    prosper_investor.run_investment_job(False)
    print('Prosper Investor completed at {}'.format(str(datetime.now())))