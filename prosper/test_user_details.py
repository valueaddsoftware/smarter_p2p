from prosper_investor import *
from datetime import *
from prosper_user import *
from prosper_api import *
from prosper_filter import *


api = ProsperApi()
users = ProsperUser.get_all_users(api)
for prosper_user in users:
  print str(prosper_user)
  print len(prosper_user.investments.loans)
  for loan in prosper_user.investments.loans:
      print loan.print_loan_details()
