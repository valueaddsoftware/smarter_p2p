from datetime import *
from prosper_user import *
from prosper_api import *
from prosper_filter import *

class ProsperInvestor:


  def __init__(self):
    self.api = ProsperApi()
    self.users = ProsperUser.get_all_users(self.api)

  def run_investment_job(self, all):
    users_who_can_invest = []
    for prosper_user in self.users:
      print str(prosper_user)
      if not prosper_user.account.can_invest():
        print "%s cannot invest" % (prosper_user.name)
      else:
        print "Adding %s into users who can invest" %  (prosper_user.name)
        users_who_can_invest.append(prosper_user)

    if not users_who_can_invest:
      print "There are no users that can invest"
      return

    start_time = datetime.now()
    while (datetime.now() - start_time) < timedelta(seconds=240): #30
      self.invest_in_listings(all, users_who_can_invest)

  def invest_in_listings(self, all, users_who_can_invest):
    listings = ProsperLoans(self.api.get_listings(self.users[0], ProsperFilter.prosper_filter))
    #listings.add_loans()
    print "Filtered number of listings: %s at time: %s" % (len(listings.loans), str(datetime.now()))
    #For each listing, see if user can be invested

    for listing in listings.loans:
      print "Found listing %s - %s, %s, %s, %s, %s" % (listing.listing_number, listing.grade, listing.annual_income, listing.purpose, listing.inquiries_in_last_6_months, listing.employment_length)

      for user in users_who_can_invest:
        if user.investments.contains_loan(listing.listing_number):
          print "%s already invested in listing %s" % (user.name, listing.listing_number)
          continue
        print "Adding listing %s to order for user %s" % (listing.listing_number, user.name)
        user.add_to_order(listing)

    for user in users_who_can_invest:
      user.complete_order()
