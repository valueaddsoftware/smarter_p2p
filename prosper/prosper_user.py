from prosper_account import *
from prosper_loans import *
from prosper_auth import *

class ProsperUser:

  #attr_reader :name, :account_id, :authorization, :investments, :investment_amount, :account

  def __init__(self, name, investment_amount, prosper_api, max_account_investment, authorization_payload):
  #(self, name, account_id, authorization, portfolio_id, investment_amount, lc_api, max_account_investment):
    self.prosper_api = prosper_api
    self.name = name
    #self.account_id = account_id
    #self.authorization = authorization
    self.authorization = ProsperAuth(self.prosper_api.retrieve_oauth_access_token(authorization_payload))
    self.investments = ProsperLoans(self.prosper_api.get_invested_listings(self))
    #print "{0} has {1} investments".format(self.name, len(self.investments.loans))
    #for loan in self.investments.loans:
    #  loan.print_loan_details()
    self.investment_amount = investment_amount  
    self.account = ProsperAccount(prosper_api.get_account(self), investment_amount, max_account_investment)
    #self.portfolio_id = portfolio_id
    self.order = []
  

  def add_order_to_investments(self):
    "Print adding current order to investments"
    self.investments.add_loans( self.order )
    self.order = [] 

  def add_to_order(self, loan):
    self.order.append(loan)  

  def generate_order_form_data(self):
    #list(map((lambda x: x **2), items))
    return { "bid_requests" : list(map((lambda loan: { "listing_id" : loan.listing_number, "bid_amount" : self.investment_amount}), self.order)) } 

  def complete_order(self):
    if not self.order:
      #print "User %s has an empty order" % (self.name)
      return
    response = self.prosper_api.place_order(self, self.generate_order_form_data())
    print "%s tried to place order and received response %s" % (self.name, response)
    self.add_order_to_investments()

  def __str__(self):
    return "User %s - %s" % (self.name, str(self.account))    

  @staticmethod
  def get_all_users(prosper_api):
    users = [ProsperUser("Varun", 25, prosper_api, None, {"grant_type" : "password", "username" : "varun.bandi@gmail.com", "password" : "d5a7NoJguBX233673ZC2"})]
    #, "28792371", "D4WOiDX3jN1rWOv5mD8D+d1J55Y=", 52062023, 25, lc_api, None),
     #        LcUser("Colin", "49566116", "/4SGNo4MnowkfEfx6JVt1X1qF8c=", 52052020, 25, lc_api, None),
      #       LcUser("Jeff", "57514829", "zYZf++5VJX/6Bi3J/x4Z/tYGMnM=", 56412024, 25, lc_api, 3000),
       #      LcUser("Eric", "1891094", "7FZviIQED0MewQbcDDR/okupJPE=", 56412026, 25, lc_api, None)]
    return users
    
  
  
