from prosper_api import *
from prosper_investor import *
from prosper_user import *
from datetime import *


start_time = datetime.now()    
api = ProsperApi()
users = ProsperUser.get_all_users(api)

prior_run_prosper_loans = None
prosper_loans = ProsperLoans()
num_loans = len(prosper_loans.loans)
offset = 0
while (datetime.now() - start_time) < timedelta(seconds=600): #30
  prosper_loans.add_loans(api.get_listings(users[0], {}, {'limit' : "50", 'offset' : str(offset)}))
  print "There are %s loans" % len(prosper_loans.loans)
  if len(prosper_loans.loans) == num_loans:
      print "no more loans found"
      
      #Check for duplicates
      if prior_run_prosper_loans is None:
          print "This is the first run"
      else:
          for index, loan in enumerate(prosper_loans.loans):
              print "Checking loan number %s out of %s" % (index, len(prosper_loans.loans)) 
              if(not prior_run_prosper_loans.contains_loan(loan.listing_number)):
                  print "FOUND new loan from previous run"         
      
      offset = 0
      prior_run_prosper_loans = prosper_loans
      prosper_loans = ProsperLoans()
      num_loans = len(prosper_loans.loans)
  else:
      num_loans = len(prosper_loans.loans)
      offset += 50    
  
    
'''''
def lambda_handler(event, context):
  print('Running Prosper loan relase analyzer at {}...'.format(event['time']))
  loan_analyzer()
  print('Prosper loan release analyzer completed at {}'.format(str(datetime.now())))

  
def loan_analyzer():
'''