from prosper_loan import *

class ProsperLoans:

  def __init__(self, new_loans=[]):
    self.loans = list(map((lambda loan: ProsperLoan(loan)), new_loans))

  def contains_loan(self, listing_number):    
    for loan in self.loans:
      if loan.has_listing_number(listing_number):
        print "Found already invested loan %s" % listing_number
        return True
        
  def add_loans (self, new_loans):
    self.loans += new_loans