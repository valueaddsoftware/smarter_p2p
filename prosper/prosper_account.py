#require '../common/account.rb'
from account import *

class ProsperAccount(Account):

  def __init__(self, account, per_listing_investment_amount, max_account_investment_threshold):
    Account.__init__(self, account.get('available_cash_balance'), account.get('pending_investments_primary_market'), account.get('outstanding_principal_on_active_notes'), account.get('total_account_value'), per_listing_investment_amount, max_account_investment_threshold)
    self.account_details = account
