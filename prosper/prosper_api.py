import urllib2
import urllib
import json

class ProsperApi:

  root_url = 'https://api.prosper.com/v1/'
  base_authorization = "D4WOiDX3jN1rWOv5mD8D+d1J55Y="
  

  def retrieve_oauth_access_token(self, authorization_payload):
    url_verb = "security/oauth/token"
    authorization_payload.update({"client_id" : "9b6462acdc144c9a97c8475a713daec7", "client_secret" : "f60a12afbe5046c2b5671100eff8935f"})
    return json.loads(self.post_with_basic_auth(url_verb, urllib.urlencode(authorization_payload), { 'accept': "application/json", 'content-type': "application/x-www-form-urlencoded" }))


  def get_invested_listings(self, prosper_user, limit_and_offset={'limit' : "1000", 'offset' : "0"}):
    url_verb = "search/mylistings/"
    return self.get_with_basic_auth(url_verb, prosper_user.authorization, limit_and_offset)["result"]

  def get_listings(self, prosper_user, prosper_filter, limit_and_offset={'limit' : "50", 'offset' : "0"}):
    url_verb = "search/listings/"
    prosper_filter.update(limit_and_offset)
    return self.get_with_basic_auth(url_verb, prosper_user.authorization, prosper_filter)["result"]

  def get_account(self, prosper_user):
    url_verb = "accounts/prosper/"
    return self.get_with_basic_auth(url_verb, prosper_user.authorization)
    
  def place_order(self, prosper_user, orders):
    url_verb = "orders/"
    print orders
    return self.post_with_basic_auth(url_verb, json.dumps(orders), { 'accept': "application/json", 'content-type': "application/json", 'authorization' : "bearer "+prosper_user.authorization.access_token})

  def get_with_basic_auth(self, url_verb, prosper_auth, parameters=None):
    api_url = self.root_url + url_verb
    api_url += '?' + urllib.urlencode(parameters) if parameters else ''
    req = urllib2.Request(api_url, headers={'accept': "application/json",'authorization' : "bearer "+prosper_auth.access_token})
    return json.load(urllib2.urlopen(req))


  def post_with_basic_auth(self, url_verb, form_data, headers):
    req = urllib2.Request(self.root_url + url_verb, form_data, headers)
    try:
      resp = urllib2.urlopen(req)
      return resp.read()   
    except urllib2.HTTPError, error:
      contents = error.read()
      print "There was an error"
      print contents

    #return urllib2.urlopen(req)



'''
  def add_headers(self, authorization, req):
    req.add_header("Authorization" , authorization)
    req.add_header("X-LC-Application-Key" , "2TNqeLPckGhsWGvw5NOw1kHpJ/8=")

  def invest_in_listings(self, lc_user, orders):
    url_verb = "accounts/" + lc_user.account_id + "/orders"
    return self.post_with_basic_auth(url_verb, lc_user.authorization, {'aid' : lc_user.account_id, 'orders' : orders})
  

  def create_portfolio(self, lc_user, portfolio_name, portfolio_description):
    url_verb = "accounts/" + lc_user.account_id + "/portfolios"
    return self.post_with_basic_auth(url_verb, lc_user.authorization, {'aid' : lc_user.account_id, 'portfolioName' : portfolio_name, 'portfolioDescription' : portfolio_description})
'''

