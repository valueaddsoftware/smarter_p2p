

class Account:
	#attr_accessor :available_balance, :pending_investment_value, :active_investment_value,
	#	:total_account_value, :per_listing_investment_amount, :max_account_investment,
  #  :api_user_id, :api_key
	#:pending_quick_invest_orders, :TotalPrincipalReceivedOnActiveNotes, :OutstandingPrincipalOnActiveNotes

  def __init__(self, available_balance, pending_investment_value, active_investment_value,
    total_account_value, api_user_id, api_key, per_listing_investment_amount, max_account_investment):
    self.available_balance = available_balance
    self.pending_investment_value = pending_investment_value
    self.active_investment_value = active_investment_value
    self.total_account_value = total_account_value
    self.per_listing_investment_amount = per_listing_investment_amount
    self.max_account_investment = total_account_value if max_account_investment is None else max_account_investment

  def can_invest(self):
    return True if self.available_balance > self.per_listing_investment_amount and 
      (self.total_account_value - self.available_balance ) <= (self.max_account_investment - self.per_listing_investment_amount)
